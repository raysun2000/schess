import Vue from 'vue';
import Router from 'vue-router';
import Board from '@/components/Board';
// import Chess from '@/components/Chess';
// import HomePage from '../components/HomePage';
import LoginPage from '../components/LoginPage';
import axios from 'axios';


// Chess API Host
axios.defaults.baseURL = 'http://localhost:5000/';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Board },
    { path: '/login', component: LoginPage },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next({ 
      path: '/login', 
      query: { returnUrl: to.path } 
    });
  }

  next();
})

export default router;

// export default new Router({
//   routes: [
//     {
//       path: '/',
//       name: 'Chess',
//       component: Chess,
//     },
//   ],
//   mode: 'history',
// });

