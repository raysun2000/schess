import chess
import chess.svg
from game import Game


class BoardImage:
    def __init__(self, css=None):
        self.css = css

    def make_svg(self, game: Game, size=360, last_move=None, check=None, orientation='white'):
        fen = game.board.fen()
        arrows = game.moves[0] if len(game.moves) == 1 else ''

        print(fen)
        parts = fen.replace("_", " ").split(" ", 1)
        board = chess.BaseBoard("/".join(parts[0].split("/")[0:8]))
        size = min(max(size, 16), 1024)
        lastmove = chess.Move.from_uci(last_move) if last_move else None
        check = chess.SQUARE_NAMES.index(check) if check else None

        def arrow(s):
            tail = chess.SQUARE_NAMES.index(s[:2])
            head = chess.SQUARE_NAMES.index(s[2:]) if len(s) > 2 else tail
            return chess.svg.Arrow(tail, head)

        arrows = [arrow(s.strip()) for s in arrows.split(",") if s.strip()]
        # flipped = orientation == "black"

        return chess.svg.board(board,
                               coordinates=False,
                               flipped=True,
                               lastmove=lastmove,
                               check=check,
                               arrows=arrows,
                               size=size,
                               style=self.css)
