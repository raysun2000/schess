export function authHeader() {
    // return authorization header with basic auth credentials
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.authdata) {
        return {
            headers: { 
                'Authorization': 'Basic ' + user.authdata 
            },
            withCredentials: true
        };
    } else {
        return {};
    }
}