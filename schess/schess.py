from pprint import pprint, pformat

from flask import Flask, jsonify, session, url_for
# from schess import get_model, oauth2
from schess import get_model
from flask import Blueprint, current_app, redirect, render_template, request, \
    session, url_for

from flask_cors import CORS, cross_origin


from .game import Game

import logging




schess = Blueprint('crud', __name__)


# app = Flask(__name__)
logging.getLogger('flask.app').setLevel(logging.INFO)


origins = ['http://localhost:8080', 'http://localhost:5000', 'http://secret-chess.firebaseapp.com', 'https://secret-chess.firebaseapp.com']
CORS(schess, supports_credentials=True, origins=origins)


moves = []


def check_credentials(username, password):
    # print('Login as...', username)
    user = db.user.find_one({'name': username})
    session['username'] = username
    print('User', user['name'])
    return True

basic_auth = BasicAuth(schess)
basic_auth.check_credentials = check_credentials

@schess.route('/users/authenticate/', methods=['GET', 'POST'])
@basic_auth.required
def auth():
    if 'username' in session:
        return jsonify({
            'name': session['username']
        })


def load_game(id=None) -> Game:
    print('Loading game')
    username = session.get('username')

    game_dict = get_model().load_game(username, id)
    game = Game(from_game_dict=game_dict)

    session['id'] = str(game._id)
    if username not in game.players:
        game.players.append(username)

    return game


def get_svg(game: Game):
    print('Get SVG')
    resp = {
        'success': True,
        'fen': game.fen,
        'players': game.players
    }
    # resp.update(game.as_dict(with_id=False))
    # pprint(resp)
    return resp


def save_game(game):
    print('Update Game')
    pprint(game)
    get_model().save_game(game)


@schess.route('/board/')
@schess.route('/board/<position>/')
@schess.route('/board/id/<id>/')
@basic_auth.required
def board(position=None, id=None):
    print('Route: Board')
    print('Get Game ID', id)

    if id:
        session['id'] = id
    current_session_id = session.get('id')

    print('current session id')
    print(current_session_id)

    game = load_game(id=current_session_id)
    print(game)

    if not position:
        return jsonify(get_svg(game))

    # game.moves.append(position)

    # if len(game.moves) == 2:
    # move = ''.join(game.moves)
    # print(move)
    if game.board.is_move_legal(position):
        real_move = chess.Move.from_uci(position)
        game.board.make_move(real_move)
        game.move_history.append(position)
    game.moves = []

    print(game)
    save_game(game)
    return jsonify(get_svg(game))


@schess.route('/games/')
@basic_auth.required
def get_games():
    print('List Games')

    games = []
    for game_dict in storage.game.find({}).limit(4).sort('created', pymongo.DESCENDING):
        game_dict['_id'] = str(game_dict['_id'])
        game_dict['href'] = '/board/id/' + game_dict['_id'] + '/'
        pprint(game_dict)
        games.append(game_dict)

    print(games)
    return jsonify({'games': games})


@schess.route('/new/', methods=['GET', 'POST'])
@basic_auth.required
def new_game():
    print('Route: New')
    game = Game()
    game.add_player(session['username'])
    d = game.as_dict()
    pprint(d)
    result = storage.game.insert_one(d)
    session['id'] = str(result.inserted_id)

    return jsonify(get_svg(game))


# app.run(debug=True)
