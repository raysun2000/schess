from pprint import pformat
from datetime import datetime
from .board import SBoard


class Game:
    def __init__(self, from_game_dict=None):
        d = from_game_dict if from_game_dict else {}
        self._id = d.get('_id')
        self.created = d.get('created', datetime.utcnow())
        self.board = SBoard(d.get('fen'))
        self.moves = d.get('moves', [])
        self.move_history = d.get('move_history', [])
        self.players = d.get('players', [])

    @property
    def fen(self):
        fen_text = self.board.fen()
        if ' w ' in fen_text:
            return fen_text.replace(' w ', ' b ')
        else:
            return fen_text.replace(' b ', ' w ')

    def add_player(self, username):
        self.players.append(username)

    def as_dict(self, with_id=True):
        d = {'created': self.created,
            'fen': self.board.fen(),
            'moves': self.moves,
            'move_history': self.move_history,
            'players': self.players
        }
        if self._id and with_id:
            d['_id'] = self._id
        return d

    def __repr__(self):
        return pformat(self.as_dict())