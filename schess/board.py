import random
import chess

WHITE_PIECES = 'KQQRRBBBNNNPPPPP'
BLACK_PIECES = WHITE_PIECES.lower()


class SBoard(chess.Board):
    def __init__(self, fen=None):
        if fen:
            super().__init__(fen)
            pm = self.piece_map()
            self.pieces = []
            for i in range(32):
                piece = pm.get(i)
                if piece:
                    self.pieces.append(piece.symbol())
                else:
                    self.pieces.append('')
        else:
            super().__init__(None)
            self.pieces = random.sample(WHITE_PIECES + BLACK_PIECES, len(WHITE_PIECES + BLACK_PIECES))
            self.update_real_pieces()

    def update_real_pieces(self):
        chess_pieces = [chess.Piece.from_symbol(s) if s else None for s in self.pieces]
        for square, piece in zip(chess.SQUARES[:32], chess_pieces):
            if piece:
                self.set_piece_at(square, piece)
            else:
                self.remove_piece_at(square)

    def make_move(self, move: chess.Move):
        self.pieces[move.to_square] = self.pieces[move.from_square]
        self.pieces[move.from_square] = ''
        self.update_real_pieces()

    def is_move_legal(self, move):
        posit = ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1", "a2", "b2", "c2", "d2", "e2", "f2", "g2", 'h2', 'a3',
                 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3', 'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4']

        pcvalues = dict(K=6, Q=5, R=4, B=3, N=2, P=1, k=6, q=5, r=4, b=3, n=2, p=1)
        pcvalues[""] = 0
        eh = 0
        eh2 = 0
        for i in range(len(posit)):
            if posit[i] == move[0] + move[1]:
                eh = i
            if posit[i] == move[2] + move[3]:
                eh2 = i
        if self.pieces[eh] == "":
            return(False)
        if self.pieces[eh2] == "" or (self.pieces[eh] in WHITE_PIECES and self.pieces[eh2] in BLACK_PIECES) or (self.pieces[eh] in BLACK_PIECES and self.pieces[eh2] in WHITE_PIECES):
            if (pcvalues[self.pieces[eh]] == 2) and (abs(int(move[1])-int(move[3])) > 1 or (abs(ord(move[0])-ord(move[2])) > 1)):
                #and (abs(int(move[1])-int(move[3]) != 0))
                #and abs(ord(move[0])-ord(move[2]))) != 0\
                print(pcvalues[self.pieces[eh]])
                between = 0
                print("the meaning of life")
                if abs(int(move[1])-int(move[3])) > 1:
                    for i in range(int(move[1])-int(move[3])-1):
                        if self.pieces[ord(move[0])+i - 97 + move[1]] != "":
                            between += 1
                else:
                    for i in range(int(abs(ord(move[0])-ord(move[2])))-1):
                        if self.pieces[int(ord(move[2])-96 + int(move[3])+i)] != "":
                            print(self.pieces[int(ord(move[2])-96 + int(move[3])+i)])
                            between += 1
                print(between)
                if between == 1:
                    return True
                else:
                    return False
            else:
                if pcvalues[self.pieces[eh]] >= pcvalues[self.pieces[eh2]] or (pcvalues[self.pieces[eh]] == 1 and pcvalues[self.pieces[eh2]] == 6):
                    if abs(int(move[1])-int(move[3]))==1 or abs(ord(move[0])-ord(move[2])) == 1:
                        return(True)
                    else:
                        return(False)
                else:
                    return(False)
        else:
            return(False)


if __name__ == '__main__':
    board = SBoard()
    while True:
        print(board)

        move = input()
        if board.is_move_legal(move):
            board.make_move(chess.Move.from_uci(move))
